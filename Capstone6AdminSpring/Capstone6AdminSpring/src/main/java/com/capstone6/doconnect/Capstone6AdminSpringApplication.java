package com.capstone6.doconnect;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableEurekaClient
@EnableSwagger2
public class Capstone6AdminSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(Capstone6AdminSpringApplication.class, args);
		System.out.println("running on port no : 8082");
	}
	
	@Bean						
	public Docket api() {
		System.out.println("admin object created..");
		return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com")).build();
	}

}
