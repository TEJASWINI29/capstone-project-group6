package com.capstone6.doconnect.repositry;

import org.springframework.data.jpa.repository.JpaRepository;

import com.capstone6.doconnect.model.UserDetails;


public interface UserRepositry extends JpaRepository<UserDetails, Integer>{

	public UserDetails findByEmailAndPassword(String email,String password);

	public UserDetails findByEmail(String email);
	
}
